import logo from "./logo.svg";
import "./App.css";
import PhoneShop from "./PhoneShop/PhoneShop";

function App() {
  return (
    <div className="App">
      <PhoneShop />
    </div>
  );
}

export default App;
