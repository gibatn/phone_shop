import React, { Component } from "react";
import Header from "./Header";
import Slider from "./Slider";
import ProductList from "./ProductList";
import Footer from "./Footer";
import Modal from "./Modal";
import dataPhone from "../Data/phoneData.json";
export default class PhoneShop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: dataPhone,
      modalPhone: {},
    };
  }
  xemChiTiet = (item) => {
    this.setState({ modalPhone: item });
  };
  render() {
    return (
      <div>
        <Header />
        <Slider />
        <div className="container mx-auto">
          {" "}
          <ProductList xemChiTiet={this.xemChiTiet} phones={this.state.data} />
        </div>
        <Modal modalPhone={this.state.modalPhone} />
        <Footer />
      </div>
    );
  }
}
