import React, { Component } from "react";
import Product from "./Product";

export default class ProductList extends Component {
  renderPhone = () => {
    let { xemChiTiet } = this.props;

    return this.props.phones.map((item) => {
      return <Product xemChiTiet={xemChiTiet} phone={item} />;
    });
  };

  render() {
    return (
      <div className="container">
        <h3 className="text-center">BEST SMARTPHONE</h3>
        <div className="row">{this.renderPhone()}</div>
      </div>
    );
  }
}
